//
// CLOCK
//

const clockElement = document.getElementById('clock');
const timeButton = document.getElementById("time-button")

init()

function init() {
//	timeButton.addEventListener("click",updateClock);
	//	timeButton.addEventListener("click",updateClockTimeoutCallback);
//	timeButton.addEventListener("click", updateClockPromise);
	timeButton.addEventListener("click",updateClockAsync);
};

function getTime() {
	const date = new Date();
	return `${formatNumber(date.getHours())}:${formatNumber(date.getMinutes())}:${formatNumber(date.getSeconds())}`;
}

// prepend 0 if number is only one digit
function formatNumber(number) {
	return number.toString().padStart(2, '0')
}

function setClock(contents) {
	clockElement.innerText = contents;
}

// version one: get the time and set it to the clock immediatly
function updateClock() {
	let time = getTime();
	setClock("Thinking...")
	setClock(time)
}

// version two
// update the clock after a timeout, using a callback handler
// 1. setTimeout is called, the timer starts to run
// 2. setClock("Thinking...")
// 3. When the timer expires the callback handler is called (timer is set to clock)
function updateClockTimeoutCallback() {
	setTimeout(() => {
		let time = getTime();
		setClock(time);
	}, 1000)
	setClock("Thinking...")
}

// version three
// update the clock using a promise

// first we write a function that wraps setTimeOut in a Promise
// when the timeout expires
// the promise is resolved with the result of the handler: resolve(handler())
// You do not need to	 be able to write this function
// The function does the same as setTimeOut, but it returns a Promise
// We use it to show it in the function calls below
// to demonstrate using a function that returns a promise
function setTimeoutPromise(handler, time) {
	return new Promise(resolve =>
		setTimeout(() => resolve(handler()), time)
	)
}

// Now use a promise to retrieve the time
// 1. setTimeoutPromise is called, this calls setTimeOut, the timer starts to run
// 2. setClock("Thinking...")
// 3. When the timer expires the promise is fulfilled and the then part is executed
function updateClockPromise() {
	setTimeoutPromise(getTime, 1000)
		.then(setClock)
	setClock("Thinking...")
}

// version 4
// Update the clock using async await
// 1. setClockNow is started. It is async and returns immediatly
// 2. setClock("Thinking...")
function updateClockAsync() {
	setClockToNow();
	setClock("Thinking...")
}

// 1. setTimeoutPromise is called, this calls setTimeOut, the timer starts to run.
//    The code blocks until the promise is fulfilled
// 2. when the promise is fulfilled the value is assigned to time
// 3. setClock is called
async function setClockToNow() {
	let time = await setTimeoutPromise(getTime, 1000);
	setClock(time);
}

