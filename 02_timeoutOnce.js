//
// CLOCK
//

const clockElement = document.getElementById('clock');
const timeButton = document.getElementById("time-button")

init()

function init() {
	timeButton.addEventListener("click",updateClock);
};

function getTime() {
	const date = new Date();
	return `${formatNumber(date.getHours())}:${formatNumber(date.getMinutes())}:${formatNumber(date.getSeconds())}`;
}

// prepend 0 if number is only one digit
function formatNumber(number) {
	return number.toString().padStart(2, '0')
}

function setClock(contents) {
	clockElement.innerText = contents;
}

// version one: get the time and set it to the clock immediatly
function updateClock() {
	let time = getTime();
	setClock("Thinking...")
	setClock(time)
}
